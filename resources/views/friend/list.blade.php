@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    @if ($userId !== auth()->id())
                        <a href="{{ route('user.show', ['id' => $userId]) }}">Back to user page</a>
                    @else
                        <a href="{{ route('user.profile', ['id' => $userId]) }}">Back to user page</a>
                    @endif

                    <hr>
                    <h3>All friends</h3>
                </div>

                <div class="card-body">
                    @if(!empty($friends))
                        @foreach($friends as $friend)
                            <div class="card mb-4">
                                <div class="row no-gutters">
                                    <div class="col-md-2">
                                        <svg class="bd-placeholder-img" width="100%" height="150" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image">
                                            <title>Placeholder</title>
                                            <rect width="100%" height="100%" fill="#868e96"></rect>
                                            <text x="40%" y="50%" fill="#dee2e6" dy=".3em">Image</text>
                                        </svg>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="card-body">
                                            <h5 class="card-title">
                                                @if ($friend->getId() !== auth()->id())
                                                    <a href="{{ route('user.show', ['id' => $friend->getId()]) }}"><b>{{ $friend->getFullName() }}</b></a>
                                                @else
                                                    <a href="{{ route('user.profile', ['id' => auth()->id()]) }}"><b>{{ $friend->getFullName() }}</b></a>
                                                @endif
                                            </h5>
                                            <p class="card-text">{{ $friend->getCity() }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="row">
                            <div class="card-body text-center col-mb-12">
                                <h5 class="card-title">No friends</h5>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
