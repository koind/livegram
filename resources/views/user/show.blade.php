@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h4>{{ $user->getFullName() }}</h4></div>

                    <div class="card-body">
                        @if(session('status'))
                            <div class="alert alert-info">
                                {{ session('status') }}
                            </div>
                        @endif
                        <table class="table table-bordered table-striped mb-4">
                            <thead>
                            <tr>
                                <th style="width: 150px;">Names</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($user)
                                @foreach($user->getUserData() as $key => $value)
                                    <tr style="word-break: break-all;">
                                        <td>{{ $key }}</td>
                                        <td>{{ $value }}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                        <div class="card mb-4">
                            <div class="card-body">
                                <h5 class="card-title">Friends</h5>
                                <p class="card-text">You can see friends by clicking on the button.</p>
                                <a href="{{ route('friend.list', ['id' => $user->getId()]) }}" class="btn btn-info">View friends</a>
                            </div>
                        </div>

                        @if ($user->getId() !== auth()->id())
                            @if (!$friendship)
                                {{ Form::open(['route' => 'friend.add', 'method' => 'POST']) }}
                                {{ Form::hidden('userId', $user->getId()) }}
                                {{ Form::submit('Add as friend', ['class' => 'btn btn-primary']) }}
                                {{ Form::close() }}
                            @else
                                @if ($friendship->isFriend())
                                    <div class="card">
                                        <div class="card-header">Your friend</div>
                                        <ul class="list-group list-group-flush">
                                            <li class="list-group-item">
                                                {{ Form::open(['route' => 'friend.delete', 'method' => 'DELETE']) }}
                                                {{ Form::hidden('userId', $user->getId()) }}
                                                {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                                {{ Form::close() }}
                                            </li>
                                        </ul>
                                    </div>
                                @else
                                    @if ($friendship->isRequestAsFriendFromMe())
                                        <div class="card">
                                            <div class="card-header">Friend request sent from me</div>
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item">
                                                    {{ Form::open(['route' => 'friend.delete', 'method' => 'DELETE']) }}
                                                    {{ Form::hidden('userId', $user->getId()) }}
                                                    {{ Form::submit('Cancel request', ['class' => 'btn btn-secondary']) }}
                                                    {{ Form::close() }}
                                                </li>
                                            </ul>
                                        </div>
                                    @else
                                        <div class="card">
                                            <div class="card-header">Friend request sent for me</div>
                                            <ul class="list-group list-group-flush">
                                                <li class="list-group-item card-columns">
                                                    {{ Form::open(['route' => 'friend.delete', 'method' => 'DELETE',]) }}
                                                    {{ Form::hidden('userId', $user->getId()) }}
                                                    {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                                    {{ Form::close() }}

                                                    {{ Form::open(['route' => 'friend.confirm', 'method' => 'POST',]) }}
                                                    {{ Form::hidden('userId', $user->getId()) }}
                                                    {{ Form::submit('Accept', ['class' => 'btn btn-success']) }}
                                                    {{ Form::close() }}
                                                </li>
                                            </ul>
                                        </div>
                                    @endif
                                @endif
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
