@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Search') }}</div>

                    <div class="card-body">
                        <form method="GET" action="{{ route('user.search') }}">
                            <div class="form-group row">
                                <label for="fullName" class="col-md-2 col-form-label">{{ __('Full name') }}</label>

                                <div class="col-md-8">
                                    <input id="fullName" type="text" class="form-control" name="fullName" value="{{ $fullName }}" required autocomplete="fullName" autofocus>
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Find') }}
                                    </button>
                                    <a href="{{ route('user.search') }}" class="btn btn-success">Clear</a>
                                </div>
                            </div>
                        </form>

                        @if(!empty($paginator) && $users = $paginator->items())
                            @foreach($users as $user)
                                <div class="card mb-4">
                                    <div class="row no-gutters">
                                        <div class="col-md-2">
                                            <svg class="bd-placeholder-img" width="100%" height="150" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image">
                                                <title>Placeholder</title>
                                                <rect width="100%" height="100%" fill="#868e96"></rect>
                                                <text x="40%" y="50%" fill="#dee2e6" dy=".3em">Image</text>
                                            </svg>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="card-body">
                                                <h5 class="card-title"><a href="{{ route('user.show', ['id' => $user->id]) }}"><b>{{ $user->name . ' ' . $user->surname }}</b></a></h5>
                                                <p class="card-text">{{ $user->city }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            {{ $paginator->links() }}
                        @else
                            <div class="row">
                                <div class="card-body text-center col-mb-12">
                                    <h5 class="card-title">Not found</h5>
                                    <p class="card-text">Sorry, could not find a user with this full name.</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
