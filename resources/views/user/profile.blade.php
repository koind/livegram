@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if(session('status'))
                    <div class="alert alert-info">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">{{ __('Profile') }}</div>

                    <div class="card-body">
                        <table class="table table-bordered table-striped mb-4">
                            <thead>
                            <tr>
                                <th style="width: 150px;">Names</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if ($user)
                                @foreach($user->getProfileData() as $key => $value)
                                    <tr style="word-break: break-all;">
                                        <td>{{ $key }}</td>
                                        <td>{{ $value }}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                        <div class="card mb-4">
                            <div class="card-body">
                                <h5 class="card-title">Friends</h5>
                                <p class="card-text">You can see friends by clicking on the button.</p>
                                <a href="{{ route('friend.list', ['id' => $user->getId()]) }}" class="btn btn-info">View friends</a>
                            </div>
                        </div>

                        <div class="card bg-light mb-4">
                            <div class="card-body">
                                <h5 class="card-title">Friends request from me</h5>
                                <div class="card-columns">
                                    @if ($requestsFromMe)
                                        @foreach($requestsFromMe as $friend)
                                            <div class="card">
                                                <img class="card-img-top" data-src="holder.js/100px180/" alt="100%x180"
                                                     src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22244%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20244%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1718a472f56%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A12pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1718a472f56%22%3E%3Crect%20width%3D%22244%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2291.3046875%22%20y%3D%2295.55%22%3E244x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                                     data-holder-rendered="true"
                                                     style="height: 180px; width: 100%; display: block;">
                                                <div class="card-body">
                                                    <h5 class="card-title"><a href="{{ route('user.show', ['id' => $friend->getId()]) }}">{{ $friend->getFullName() }}</a></h5>
                                                    <p class="card-text">{{ $friend->getInterests() }}</p>
                                                </div>
                                                <div class="card-footer">
                                                    {{ Form::open(['route' => 'friend.delete', 'method' => 'DELETE']) }}
                                                    {{ Form::hidden('userId', $friend->getId()) }}
                                                    {{ Form::submit('Cancel request', ['class' => 'btn btn-secondary']) }}
                                                    {{ Form::close() }}
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <p>List empty</p>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="card text-white bg-dark mb-4">
                            <div class="card-body">
                                <h5 class="card-title">Friends request for me</h5>
                                <div class="card-columns">
                                    @if ($requestsForMe)
                                        @foreach($requestsForMe as $friend)
                                            <div class="card text-black-50">
                                                <img class="card-img-top" data-src="holder.js/100px180/" alt="100%x180"
                                                     src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22244%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20244%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_1718a472f56%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A12pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_1718a472f56%22%3E%3Crect%20width%3D%22244%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2291.3046875%22%20y%3D%2295.55%22%3E244x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                                     data-holder-rendered="true"
                                                     style="height: 180px; width: 100%; display: block;">
                                                <div class="card-body">
                                                    <h5 class="card-title"><a href="{{ route('user.show', ['id' => $friend->getId()]) }}">{{ $friend->getFullName() }}</a></h5>
                                                    <p class="card-text">{{ $friend->getInterests() }}</p>
                                                </div>
                                                <div class="card-footer row">
                                                    {{ Form::open(['route' => 'friend.delete', 'method' => 'DELETE']) }}
                                                    {{ Form::hidden('userId', $friend->getId()) }}
                                                    {{ Form::submit('Delete', ['class' => 'btn btn-danger col-md-12']) }}
                                                    {{ Form::close() }}

                                                    {{ Form::open(['route' => 'friend.confirm', 'method' => 'POST', 'class' => 'ml-3']) }}
                                                    {{ Form::hidden('userId', $friend->getId()) }}
                                                    {{ Form::submit('Accept', ['class' => 'btn btn-success col-md-12']) }}
                                                    {{ Form::close() }}
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <p>List empty</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
