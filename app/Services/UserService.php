<?php

namespace App\Services;

use App\Entities\User;
use App\Repositories\UserRepository;

class UserService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param array $data
     * @return User
     * @throws \Exception
     */
    public function create(array $data): User
    {
        if ($this->userRepository->hasUserByEmail($data['email'])) {
            throw new \DomainException('A user with that email already exists');
        }

        return $this->userRepository->create($data);
    }
}