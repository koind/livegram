<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $email
     * @param string $password
     * @param bool $isRemember
     * @return bool
     */
    public function attempt(string $email, string $password, bool $isRemember): bool
    {
        $user = $this->userRepository->getOneByEmail($email);

        if (empty($user)) {
            return false;
        }

        if (Hash::check($password, $user->getPassword())) {
            Auth::login($user, $isRemember);

            return true;
        }

        return false;
    }
}