<?php

namespace App\Services;

use App\Entities\Friendship;
use App\Entities\User;
use App\Repositories\FriendshipRepository;
use App\Repositories\UserRepository;

class FriendshipService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var FriendshipRepository
     */
    private $friendshipRepository;

    /**
     * UserService constructor.
     * @param UserRepository $userRepository
     * @param FriendshipRepository $friendshipRepository
     */
    public function __construct(UserRepository $userRepository, FriendshipRepository $friendshipRepository)
    {
        $this->userRepository       = $userRepository;
        $this->friendshipRepository = $friendshipRepository;
    }

    /**
     * @param int $userId
     * @param int $limit
     * @return \App\Entities\User[]|array
     */
    public function getFriends(int $userId, $limit = 200)
    {
        if (!$this->userRepository->getOneById($userId)) {
            throw new \InvalidArgumentException('User not found');
        }

        $userIds = $this->friendshipRepository->getFriendsIds($userId, $limit);

        if (empty($userIds)) {
            return [];
        }

        return $this->userRepository->findAllByIds($userIds);
    }

    /**
     * @param int $limit
     * @return \App\Entities\User[]|array
     */
    public function getRequests($limit = 200)
    {
        $result = $this->friendshipRepository->findMyRequestsByStatus(
            Friendship::FRIENDSHIP_STATUS_PENDING,
            $limit
        );

        if (empty($result)) {
            return [[], []];
        }

        $userIdsRequestAsFriendFromMe = [];
        $userIdsRequestAsFriendForMe  = [];

        $friends = $this->friendshipRepository->toFriendship($result);

        foreach ($friends as $friend) {
            if ($friend->isRequestAsFriendFromMe()) {
                $userIdsRequestAsFriendFromMe[] = $friend->getFriendId();
            } else {
                $userIdsRequestAsFriendForMe[] = $friend->getUserId();
            }
        }

        $usersRequestAsFriendFromMe = [];
        $usersRequestAsFriendForMe  = [];

        if (!empty($userIdsRequestAsFriendFromMe)) {
            $usersRequestAsFriendFromMe = $this->userRepository->findAllByIds($userIdsRequestAsFriendFromMe);
        }

        if (!empty($userIdsRequestAsFriendForMe)) {
            $usersRequestAsFriendForMe = $this->userRepository->findAllByIds($userIdsRequestAsFriendForMe);
        }

        return [$usersRequestAsFriendFromMe, $usersRequestAsFriendForMe];
    }

    /**
     * @param int $userId
     * @throws \Exception
     */
    public function add(int $userId)
    {
        if (!$this->userRepository->getOneById($userId)) {
            throw new \InvalidArgumentException('User not found');
        }

        if ($this->friendshipRepository->findRequest($userId)) {
            throw new \InvalidArgumentException(
                'You cannot add as friend because it is your friend or friend request has already been sent'
            );
        }

        $this->friendshipRepository->add($userId);
    }

    /**
     * @param int $userId
     * @throws \Exception
     */
    public function confirm(int $userId)
    {
        if (!$this->userRepository->getOneById($userId)) {
            throw new \InvalidArgumentException('User not found');
        }

        if (!$this->friendshipRepository->findRequest($userId)) {
            throw new \InvalidArgumentException('No friend request from user');
        }

        $this->friendshipRepository->confirm($userId);
    }

    /**
     * @param int $userId
     * @throws \Exception
     */
    public function delete(int $userId)
    {
        if (!$this->userRepository->getOneById($userId)) {
            throw new \InvalidArgumentException('User not found');
        }

        if (!$this->friendshipRepository->findRequest($userId)) {
            throw new \InvalidArgumentException('You have no such request or friend');
        }

        $this->friendshipRepository->delete($userId);
    }
}
