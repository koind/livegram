<?php

namespace App\Entities;

use Illuminate\Contracts\Auth\Authenticatable;

class User implements Authenticatable
{
    public const GENDER_MALE = 1;
    public const GENDER_FEMALE = 2;

    public const GENDER = [
        self::GENDER_MALE => 'Male',
        self::GENDER_FEMALE => 'Female',
    ];

    public const ALLOWED_FIELD_TO_SAVE = [
        'id',
        'name',
        'surname',
        'email',
        'age',
        'gender',
        'city',
        'interests',
        'password',
        'remember_token',
        'created_at',
        'updated_at',
    ];

    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $surname;

    /**
     * @var string
     */
    public $email;

    /**
     * @var int
     */
    public $age;

    /**
     * @var int
     */
    public $gender;

    /**
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $interests;

    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $rememberToken;

    /**
     * @var string|null
     */
    public $createdAt;

    /**
     * @var string|null
     */
    public $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return User
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     * @return User
     */
    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->getName() . ' ' . $this->getSurname();
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param int $age
     * @return User
     */
    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }

    /**
     * @return int
     */
    public function getGender(): int
    {
        return $this->gender;
    }

    /**
     * @param int $gender
     * @return User
     */
    public function setGender(int $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return User
     */
    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getInterests(): string
    {
        return $this->interests;
    }

    /**
     * @param string $interests
     * @return User
     */
    public function setInterests(string $interests): self
    {
        $this->interests = $interests;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getRememberToken()
    {
        return $this->rememberToken;
    }

    /**
     * @param string $rememberToken
     */
    public function setRememberToken($rememberToken): void
    {
        $this->rememberToken = $rememberToken;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @param string|null $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }

    /**
     * @param string|null $updatedAt
     * @return User
     */
    public function setUpdatedAt(?string $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @param array $data
     * @return User
     */
    public static function create(array $data): self
    {
        $user = new self();

        foreach ($data as $name => $value) {
            $methodName = 'set' . ucfirst($name);

            if (method_exists($user, $methodName)) {
                $user->$methodName($value);
            }

            if ($name === 'remember_token') {
                $user->setRememberToken($value);
            }

            if ($name === 'created_at') {
                $user->setCreatedAt($value);
            }

            if ($name === 'updated_at') {
                $user->setUpdatedAt($value);
            }
        }

        return $user;
    }

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return 'id';
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->{$this->getAuthIdentifierName()};
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    /**
     * @return array
     */
    public function getProfileData()
    {
        return [
            'ID' => $this->getId(),
            'Name' => $this->getName(),
            'Surname' => $this->getSurname(),
            'Email' => $this->getEmail(),
            'Age' => $this->getAge(),
            'Gender' => self::GENDER[$this->getGender()] ?? '-',
            'City' => $this->getCity(),
            'Interests' => $this->getInterests(),
        ];
    }

    /**
     * @return array
     */
    public function getUserData()
    {
        return [
            'Age' => $this->getAge(),
            'Gender' => self::GENDER[$this->getGender()] ?? '-',
            'City' => $this->getCity(),
            'Interests' => $this->getInterests(),
        ];
    }
}
