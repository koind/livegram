<?php

namespace App\Entities;

class Friendship
{
    /**
     * В ожидании
     */
    public const FRIENDSHIP_STATUS_PENDING = 0;

    /**
     * Принято
     */
    public const FRIENDSHIP_STATUS_ACCEPTED = 1;

    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $userId;

    /**
     * @var int
     */
    public $friendId;

    /**
     * @var int
     */
    public $loggedInUserId;

    /**
     * @var int
     */
    public $status;

    /**
     * @var string|null
     */
    public $createdAt;

    /**
     * @var string|null
     */
    public $updatedAt;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Friendship
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return Friendship
     */
    public function setUserId(int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * @return int
     */
    public function getFriendId(): int
    {
        return $this->friendId;
    }

    /**
     * @param int $friendId
     * @return Friendship
     */
    public function setFriendId(int $friendId): self
    {
        $this->friendId = $friendId;

        return $this;
    }

    /**
     * @return int
     */
    public function getLoggedInUserId(): int
    {
        return $this->loggedInUserId;
    }

    /**
     * @param int $loggedInUserId
     * @return Friendship
     */
    public function setLoggedInUserId(int $loggedInUserId): self
    {
        $this->loggedInUserId = $loggedInUserId;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Friendship
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->createdAt;
    }

    /**
     * @param string|null $createdAt
     * @return Friendship
     */
    public function setCreatedAt($createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updatedAt;
    }

    /**
     * @param string|null $updatedAt
     * @return Friendship
     */
    public function setUpdatedAt(?string $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @param array $data
     * @return Friendship
     */
    public static function create(array $data): self
    {
        $friendship = new self();

        foreach ($data as $name => $value) {
            $methodName = 'set' . ucfirst($name);

            if (method_exists($friendship, $methodName)) {
                $friendship->$methodName($value);
            }

            if ($name === 'user_id') {
                $friendship->setUserId($value);
            }

            if ($name === 'friend_id') {
                $friendship->setFriendId($value);
            }

            if ($name === 'created_at') {
                $friendship->setCreatedAt($value);
            }

            if ($name === 'updated_at') {
                $friendship->setUpdatedAt($value);
            }
        }

        $friendship->setLoggedInUserId(auth()->id());

        return $friendship;
    }

    /**
     * Является ли другом
     *
     * @return bool
     */
    public function isFriend(): bool
    {
        return $this->getStatus() === self::FRIENDSHIP_STATUS_ACCEPTED;
    }

    /**
     * Является ли запросом в друзья
     *
     * @return bool
     */
    public function isToFriend(): bool
    {
        return $this->getStatus() === self::FRIENDSHIP_STATUS_PENDING;
    }

    /**
     * Является ли запросом в друзья от меня
     *
     * @return bool
     */
    public function isRequestAsFriendFromMe(): bool
    {
        return $this->getLoggedInUserId() === $this->getUserId();
    }

    /**
     * Является ли запросом в друзья для меня
     *
     * @return bool
     */
    public function isRequestAsFriendForMe(): bool
    {
        return $this->getLoggedInUserId() === $this->getFriendId();
    }
}
