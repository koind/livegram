<?php

namespace App\Http\Controllers;

use App\Entities\User;
use App\Services\AuthService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var AuthService
     */
    private $authService;

    public function __construct(UserService $userService, AuthService $authService)
    {
        $this->userService = $userService;
        $this->authService = $authService;
    }

    public function registerForm()
    {
        return view('pages.register', ['gender' => User::GENDER]);
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:100',
            'surname' => 'required|max:100',
            'email' => 'required|email|max:100',
            'age' => 'required|integer|between:13,150',
            'gender' => 'required',
            'city' => 'required|max:100',
            'interests' => 'required|max:500',
            'password' => 'required|min:6|confirmed',
        ]);

        try {
            $this->userService->create($request->all());
        } catch (\DomainException $e) {
            return redirect()->back()->with('status', $e->getMessage());
        } catch (\Exception $e) {
            // write exception in graylog
            return redirect()->back()->with('status', 'There was a technical error, contact support');
        }

        return redirect('/login')->with('success', 'Congratulations! You have successfully created an account!');
    }

    public function loginForm()
    {
        return view('pages.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:100',
            'password' => 'required|min:6',
        ]);

        $email = $request->get('email');
        $password = $request->get('password');
        $isRemember = (bool) $request->get('remember');

        try {
            if ($this->authService->attempt($email, $password, $isRemember)) {
                return redirect()->route('user.profile', ['id' => Auth::id()]);
            }
        } catch (\Exception $e) {
            // write exception in graylog
            return redirect()->back()->with('status', 'There was a technical error, contact support');
        }

        return redirect()->back()->with('status', 'Incorrect email or password');
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/login');
    }
}
