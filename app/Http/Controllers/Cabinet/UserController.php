<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Repositories\FriendshipRepository;
use App\Repositories\UserRepository;
use App\Services\FriendshipService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var FriendshipRepository
     */
    private $friendshipRepository;

    /**
     * @var FriendshipService
     */
    private $friendshipService;

    /**
     * ProfileController constructor.
     * @param UserRepository $userRepository
     * @param FriendshipRepository $friendshipRepository
     * @param FriendshipService $friendshipService
     */
    public function __construct(
        UserRepository $userRepository,
        FriendshipRepository $friendshipRepository,
        FriendshipService $friendshipService
    ) {
        $this->userRepository       = $userRepository;
        $this->friendshipRepository = $friendshipRepository;
        $this->friendshipService    = $friendshipService;
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile(int $id)
    {
        if (auth()->id() !== $id) {
            abort(404);
        }

        try {
            $user = $this->userRepository->getOneById($id);

            if (empty($user)) {
                abort(404);
            }

            list($requestsFromMe, $requestsForMe) = $this->friendshipService->getRequests();
        } catch (\Exception $e) {
            abort(404);
        }

        return view('user.profile', compact('user', 'requestsFromMe', 'requestsForMe'));
    }

    /**
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show(int $id)
    {
        if ($id === auth()->id()) {
            return redirect()->back();
        }

        try {
            $user = $this->userRepository->getOneById($id);

            if (empty($user)) {
                abort(404);
            }

            $friendship = $this->friendshipRepository->findRequest($id);
        } catch (\Exception $e) {
            abort(404);
        }

        return view('user.show', compact('user', 'friendship'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $page     = $request->input('page') ?: 1;
        $fullName = trim($request->get('fullName'));
        $names    = explode(' ', $fullName);
        $path     = sprintf('%s?%s', $request->url(), http_build_query($request->except(['page'])));

        if (empty($fullName)) {
            $paginator = $this->userRepository->getAllWithLimit();
        } else {
            $firstName = $names[0] ?? '';
            $lastName  = $names[1] ?? '';
            $paginator = $this->userRepository->findByNames($firstName, $lastName, $page);
        }

        $paginator = $paginator->withPath($path);

        return view('user.search', compact('paginator', 'fullName'));
    }
}
