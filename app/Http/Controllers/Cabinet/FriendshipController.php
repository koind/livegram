<?php

namespace App\Http\Controllers\Cabinet;

use App\Http\Controllers\Controller;
use App\Services\FriendshipService;
use Illuminate\Http\Request;

class FriendshipController extends Controller
{
    /**
     * @var FriendshipService
     */
    private $friendshipService;

    /**
     * FriendController constructor.
     * @param FriendshipService $friendshipService
     */
    public function __construct(FriendshipService $friendshipService)
    {
        $this->friendshipService = $friendshipService;
    }

    /**
     * @param int $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function showFriends(int $userId)
    {
        try {
            $friends = $this->friendshipService->getFriends($userId);
        } catch (\InvalidArgumentException $e) {
            return redirect()->back()->with('status', $e->getMessage());
        } catch (\Exception $e) {
            // write exception in graylog
            return redirect()->back()->with('status', 'There was an error sending your friend request.');
        }

        return view('friend.list', compact('friends', 'userId'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function add(Request $request)
    {
        $this->validate($request, [
            'userId' => 'required|integer',
        ]);

        $userId = (int) $request->get('userId');

        try {
            $this->friendshipService->add($userId);
        } catch (\InvalidArgumentException $e) {
            return redirect()->back()->with('status', $e->getMessage());
        } catch (\Exception $e) {
            // write exception in graylog
            return redirect()->back()->with('status', 'There was an error sending your friend request.');
        }

        return redirect()->back()->with('status', 'You have successfully sent a friend request!');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function confirm(Request $request)
    {
        $this->validate($request, [
            'userId' => 'required|integer',
        ]);

        $userId = (int) $request->get('userId');

        try {
            $this->friendshipService->confirm($userId);
        } catch (\InvalidArgumentException $e) {
            return redirect()->back()->with('status', $e->getMessage());
        } catch (\Exception $e) {
            // write exception in graylog
            return redirect()->back()->with('status', 'An error occurred while confirming the request.');
        }

        return redirect()->back()->with('status', 'You have confirmed friendship with this user!');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function delete(Request $request)
    {
        $this->validate($request, [
            'userId' => 'required|integer',
        ]);

        $userId = (int) $request->get('userId');

        try {
            $this->friendshipService->delete($userId);
        } catch (\InvalidArgumentException $e) {
            return redirect()->back()->with('status', $e->getMessage());
        } catch (\Exception $e) {
            // write exception in graylog
            return redirect()->back()->with('status', 'An error occurred while performing the operation.');
        }

        return redirect()->back()->with('status', 'Request completed successfully!');
    }
}

