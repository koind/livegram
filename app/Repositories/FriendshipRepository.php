<?php

namespace App\Repositories;

use App\Entities\Friendship;
use App\Helpers\DateTimeHelper;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class FriendshipRepository
{
    public const TABLE_NAME = 'friendship';

    /**
     * @var Builder
     */
    protected $table;

    /**
     * UserRepository constructor.
     * @param DatabaseManager $db
     */
    public function __construct(DatabaseManager $db)
    {
        $this->table = $db->table(self::TABLE_NAME);
    }

    /**
     * @param int $userId
     * @return void
     * @throws \Exception
     */
    public function add(int $userId)
    {
        $data               = [];
        $data['user_id']    = auth()->id();
        $data['friend_id']  = $userId;
        $data['status']     = Friendship::FRIENDSHIP_STATUS_PENDING;
        $data['created_at'] = DateTimeHelper::getCurrentInUTC();

        $this->table->insert($data);
    }

    /**
     * @param int $userId
     * @return int
     * @throws \Exception
     */
    public function confirm(int $userId)
    {
        $data               = [];
        $data['status']     = Friendship::FRIENDSHIP_STATUS_ACCEPTED;
        $data['updated_at'] = DateTimeHelper::getCurrentInUTC();

        return DB::table(self::TABLE_NAME)
            ->where('user_id', $userId)
            ->where('friend_id', auth()->id())
            ->where('status', Friendship::FRIENDSHIP_STATUS_PENDING)
            ->update($data);
    }

    /**
     * @param int $userId
     * @return bool
     */
    public function isFriend(int $userId): bool
    {
        $result = $this
            ->table
            ->where('status', Friendship::FRIENDSHIP_STATUS_ACCEPTED)
            ->where(function ($query) use ($userId) {
                $query->where(function ($query) use ($userId) {
                    $query->where('user_id', auth()->id())
                        ->where('friend_id', $userId);
                });

                $query->orWhere(function ($query) use ($userId) {
                    $query->where('user_id', $userId)
                        ->where('friend_id', auth()->id());
                });
            })
            ->take(1)
            ->get();

        return !empty($result->all());
    }

    /**
     * @param int $id
     * @return null|Friendship
     */
    public function getOneById(int $id): ?Friendship
    {
        $result = $this->table->where('id', $id)->take(1)->get();

        if (empty($result->all())) {
            return null;
        }

        $data = (array) $result->get(0);

        return Friendship::create($data);
    }

    /**
     * @param int $userId
     * @return null|Friendship
     */
    public function findRequest(int $userId): ?Friendship
    {
        $result = $this
            ->table
            ->where(function ($query) use ($userId) {
                $query->where(function ($query) use ($userId) {
                    $query->where('user_id', auth()->id())
                        ->where('friend_id', $userId);
                });

                $query->orWhere(function ($query) use ($userId) {
                    $query->where('user_id', $userId)
                        ->where('friend_id', auth()->id());
                });
            })
            ->take(1)
            ->get();

        if (empty($result->all())) {
            return null;
        }

        $data = (array) $result->get(0);

        return Friendship::create($data);
    }

    /**
     * @param int $userId
     * @return int
     */
    public function delete(int $userId): int
    {
        return $this
            ->table
            ->where(function ($query) use ($userId) {
                $query->where(function ($query) use ($userId) {
                    $query->where('user_id', auth()->id())
                        ->where('friend_id', $userId);
                });

                $query->orWhere(function ($query) use ($userId) {
                    $query->where('user_id', $userId)
                        ->where('friend_id', auth()->id());
                });
            })
            ->delete();
    }

    /**
     * @param int $userId
     * @param int $limit
     * @return array
     */
    public function getFriendsIds(int $userId, int $limit = 100): array
    {
        $result = $this
            ->table
            ->where('status', Friendship::FRIENDSHIP_STATUS_ACCEPTED)
            ->where(function ($query) use ($userId) {
                $query->where('user_id', $userId)
                    ->orWhere('friend_id', $userId);
            })
            ->take($limit)
            ->get()
            ->all();

        if (empty($result)) {
            return [];
        }

        $userIds = [];
        $friends = $this->toFriendship($result);

        foreach ($friends as $friend) {
            if ($friend->getUserId() === $userId) {
                $userIds[] = $friend->getFriendId();
            } else {
                $userIds[] = $friend->getUserId();
            }
        }

        return $userIds;
    }

    /**
     * @param int $status
     * @param int $limit
     * @return Friendship[]
     */
    public function findMyRequestsByStatus(int $status, int $limit = 100): array
    {
        $result = $this
            ->table
            ->where('status', $status)
            ->where(function ($query) {
                $query->where('user_id', auth()->id())
                    ->orWhere('friend_id', auth()->id());
            })
            ->take($limit)
            ->get()
            ->all();

        if (empty($result)) {
            return [];
        }

        return $this->toFriendship($result);
    }

    /**
     * @param $collection
     * @return Friendship[]
     */
    public function toFriendship(array $collection): array
    {
        $users = [];

        foreach ($collection as $item) {
            $users[] = Friendship::create((array) $item);
        }

        return $users;
    }
}
