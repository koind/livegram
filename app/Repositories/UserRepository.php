<?php

namespace App\Repositories;

use App\Entities\User;
use App\Helpers\DateTimeHelper;
use App\Helpers\StringHelper;
use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Query\Builder;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class UserRepository
{
    public const TABLE_NAME = 'users';

    /**
     * @var Builder
     */
    protected $table;

    /**
     * UserRepository constructor.
     * @param DatabaseManager $db
     */
    public function __construct(DatabaseManager $db)
    {
        $this->table = $db->table(self::TABLE_NAME);
    }

    /**
     * @param array $data
     * @return User
     */
    public function create(array $data): User
    {
        $data['password']   = bcrypt($data['password']);
        $data['age']        = (int) $data['age'];
        $data['gender']     = (int) $data['gender'];
        $data['created_at'] = DateTimeHelper::getCurrentInUTC();
        $data               = StringHelper::extract($data, User::ALLOWED_FIELD_TO_SAVE);
        $data['id']         = $this->table->insertGetId($data);

        return User::create($data);
    }

    /**
     * @param string $email
     * @return bool
     */
    public function hasUserByEmail(string $email): bool
    {
        if (empty($email)) {
            return false;
        }

        $result = $this->table->where('email', $email)->get();

        return !empty($result->all());
    }

    /**
     * @param string $email
     * @return null|User
     */
    public function getOneByEmail(string $email): ?User
    {
        $result = $this->table->where('email', $email)->take(1)->get();

        if (empty($result->all())) {
            return null;
        }

        $userData = (array) $result->get(0);

        return User::create($userData);
    }

    /**
     * @param int $id
     * @return null|User
     */
    public function getOneById(int $id): ?User
    {
        $result = $this->table->where('id', $id)->take(1)->get();

        if (empty($result->all())) {
            return null;
        }

        $userData = (array) $result->get(0);

        return User::create($userData);
    }

    /**
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function getAllWithLimit(int $limit = 10): LengthAwarePaginator
    {
        $result = $this
            ->table
            ->select(['id', 'name', 'surname', 'city'])
            ->take($limit)
            ->orderBy('id')
            ->get()
            ->all();

        return new LengthAwarePaginator($result, count($result), $limit, 0);
    }

    /**
     * @param array $ids
     * @param int $limit
     * @return User[]
     */
    public function findAllByIds(array $ids, int $limit = 6): array
    {
        $result = DB::table(self::TABLE_NAME)
            ->whereIn('id', $ids)
            ->get()
            ->take($limit)
            ->all();

        if (empty($result)) {
            return [];
        }

        return $this->toUsers($result);
    }

    /**
     * @param string $firstName
     * @param string $lastName
     * @param int $page
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function findByNames(string $firstName, string $lastName, int $page, int $limit = 10): LengthAwarePaginator
    {
        $firstName .= '%';
        $lastName  .= '%';

        $query = $this
            ->table
            ->select(['id', 'name', 'surname', 'city'])
            ->where('name', 'like', $firstName)
            ->where('surname', 'like', $lastName)
            ->orderBy('id');

        $totalCount = $query->count();

        if (!$totalCount) {
            return new LengthAwarePaginator([], $totalCount, $limit, $page);
        }

        if ($page) {
            $skip     = $limit * ($page - 1);
            $rawQuery = $query->take($limit)->skip($skip);
        } else {
            $rawQuery = $query->take($limit)->skip(0);
        }

        $result = $rawQuery->get()->all();

        return new LengthAwarePaginator($result, $totalCount, $limit, $page);
    }

    /**
     * @param $collection
     * @return User[]
     */
    protected function toUsers(array $collection): array
    {
        $users = [];

        foreach ($collection as $item) {
            $users[] = User::create((array) $item);
        }

        return $users;
    }
}
