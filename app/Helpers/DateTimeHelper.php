<?php

namespace App\Helpers;

class DateTimeHelper
{
    public const UTC_TZ = 'UTC';
    public const KZ_TZ = 'Asia/Almaty';
    public const DB_FORMAT = 'c';
    public const USER_FORMAT = 'Y-m-d H:i:s';

    /**
     * @return \DateTime
     * @throws \Exception
     */
    public static function getCurrentInUTC(): \DateTime
    {
        return new \DateTime('now', new \DateTimeZone(self::UTC_TZ));
    }

    /**
     * @param \DateTime $datetime
     * @return \DateTime
     * @throws \Exception
     */
    public static function convertUTCInKZ(\DateTime $datetime): \DateTime
    {
        $datetime->setTimeZone(new \DateTimeZone(self::KZ_TZ));

        return $datetime;
    }
}