<?php

namespace App\Helpers;

class StringHelper
{
    /**
     * @param array $data
     * @param array $allowedFields
     * @return array
     */
    public static function extract(array $data, array $allowedFields): array
    {
        return array_intersect_key($data, array_flip($allowedFields));
    }
}