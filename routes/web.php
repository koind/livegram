<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'guest'], function () {
    Route::get('/register', 'AuthController@registerForm')->name('register');
    Route::post('/register', 'AuthController@register');
    Route::get('/login', 'AuthController@loginForm')->name('login');
    Route::post('/login', 'AuthController@login');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/logout', 'AuthController@logout')->name('logout');
});

Route::group(
    [
        'namespace'  => 'Cabinet',
        'middleware' => ['auth']
    ],
    function () {
        Route::group(['as' => 'user.'], function () {
            Route::get('/profile/{id}', 'UserController@profile')->name('profile');
            Route::get('/id/{id}', 'UserController@show')->name('show');
            Route::get('/search', 'UserController@search')->name('search');
        });

        Route::group(['prefix' => 'friend', 'as' => 'friend.'], function () {
            Route::get('/list/{id}', 'FriendshipController@showFriends')->name('list');
            Route::post('/add', 'FriendshipController@add')->name('add');
            Route::post('/confirm', 'FriendshipController@confirm')->name('confirm');
            Route::delete('/delete', 'FriendshipController@delete')->name('delete');
        });
    }
);
