up:
	docker-compose up -d

down:
	docker-compose down

db:
	php artisan migrate --env=local-db

seed:
	php artisan db:seed --env=local-db

