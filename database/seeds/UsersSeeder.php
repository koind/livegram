<?php

use App\Entities\User;
use App\Helpers\DateTimeHelper;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('memory_limit', '-1');
        DB::disableQueryLog();//disable log

        $start     = microtime(true);
        $userData  = [];
        $createdAt = DateTimeHelper::getCurrentInUTC();
        $passport  = bcrypt('123123');
        $faker     = Faker\Factory::create();

        for ($i = 1; $i <= 1000000; $i++) {
            $gender    = mt_rand(1, 2);
            $firstName = $gender === User::GENDER_MALE ? $faker->firstNameMale : $faker->firstNameFemale;

            $userData[] = [
                'name'       => $firstName,
                'surname'    => $faker->lastName,
                'email'      => Str::random(2) . $i . time() .'@mail.ru',
                'age'        => mt_rand(18, 55),
                'gender'     => $gender,
                'city'       => Str::random(7),
                'interests'  => Str::random(200),
                'password'   => $passport,
                'created_at' => $createdAt,
            ];
        }

        $chunks = array_chunk($userData, 5000);

        foreach ($chunks as $chunk) {
            DB::table('users')->insert($chunk);
        }

        echo 'Время выполнения скрипта: '.round(microtime(true) - $start, 4).' сек.';
    }
}
